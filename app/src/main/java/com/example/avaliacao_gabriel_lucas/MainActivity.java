package com.example.avaliacao_gabriel_lucas;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button btnLiberar;
    Button btnSalvar;
    Button btnReservarTodos;
    EditText inputNumMesa;
    List<Button> buttonList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAllFundosIds();
        setAllButton();

        for (int i = 0; i < buttonList.size(); i++) {
            int finalI = i;
            buttonList.get(i).setOnClickListener(o -> {
                buttonList.get(finalI).setEnabled(false);
                findViewById(Utils.getFundoIdByValue(finalI)).setBackgroundColor(Utils.getColorId(this, R.color.colorFundoMesaEmUso));
            });
        }

        btnLiberar.setOnClickListener(o -> {
            inputNumMesa = findViewById(R.id.input_num_mesa);
            if (inputNumMesa != null && !inputNumMesa.getText().toString().isEmpty()) {
                Integer numero = Integer.valueOf(inputNumMesa.getText().toString());

                if (numero < 1 || numero > 9) {
                    Toast.makeText(this, "Número de mesa inválido!", Toast.LENGTH_LONG).show();
                } else if (!buttonList.get(numero - 1).isEnabled()) {
                    buttonList.get(numero - 1).setEnabled(true);
                    findViewById(Utils.getFundoIdByValue(numero - 1)).setBackgroundColor(Utils.getColorId(this, R.color.colorFundoMesa));
                } else {
                    Toast.makeText(this, "Mesa não reservada. A mesa " + numero + " encontra-se habilitada para reserva!", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "Informe o número da mesa!", Toast.LENGTH_LONG).show();
            }
        });

        btnReservarTodos.setOnClickListener(o -> {
            for (int i = 0; i < buttonList.size(); i++) {
                if (buttonList.get(i).isEnabled()) {
                    buttonList.get(i).setEnabled(false);
                    findViewById(Utils.getFundoIdByValue(i)).setBackgroundColor(Utils.getColorId(this, R.color.colorFundoMesaEmUso));
                } else {
                    Toast.makeText(this, "Operação inválida. Todas as mesas já possuem reserva.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSalvar.setOnClickListener(o ->{
            buttonList.forEach(b ->{
                if(!b.isEnabled()){
                    Utils.salvar(this, Integer.toString(b.getId()), b.isEnabled());
                    Toast.makeText(this, "Salvo com sucesso!!.", Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    private void setAllFundosIds() {
        Utils.addFundoId(findViewById(R.id.fundo_um).getId());
        Utils.addFundoId(findViewById(R.id.fundo_dois).getId());
        Utils.addFundoId(findViewById(R.id.fundo_tres).getId());
        Utils.addFundoId(findViewById(R.id.fundo_quatro).getId());
        Utils.addFundoId(findViewById(R.id.fundo_cinco).getId());
        Utils.addFundoId(findViewById(R.id.fundo_seis).getId());
        Utils.addFundoId(findViewById(R.id.fundo_sete).getId());
        Utils.addFundoId(findViewById(R.id.fundo_oito).getId());
        Utils.addFundoId(findViewById(R.id.fundo_nove).getId());
    }

    private void setAllButton() {
        buttonList = Arrays.asList(findViewById(R.id.um), findViewById(R.id.dois),
                findViewById(R.id.tres),
                findViewById(R.id.quatro),
                findViewById(R.id.cinco),
                findViewById(R.id.seis),
                findViewById(R.id.sete),
                findViewById(R.id.oito),
                findViewById(R.id.nove));
        btnLiberar = findViewById(R.id.btn_liberar_mesa);
        btnSalvar = findViewById(R.id.btn_salvar_operacao);
        btnReservarTodos = findViewById(R.id.btn_reservar_todas_mesas);

        for (int i = 0; i < buttonList.size(); i++) {
            buttonList.get(i).setEnabled(Utils.getStatus(this, Integer.toString(buttonList.get(i).getId())));
            if (!buttonList.get(i).isEnabled()) {
                findViewById(Utils.getFundoIdByValue(i)).setBackgroundColor(Utils.getColorId(this, R.color.colorFundoMesaEmUso));
            }
        }
    }
}
