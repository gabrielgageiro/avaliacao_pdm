package com.example.avaliacao_gabriel_lucas;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {

    private static List<Integer> fundosIds = new ArrayList<>();

    public Utils() {
    }

    public static List<Integer> getFundosIds() {
        return fundosIds;
    }

    public static void setFundosIds(List<Integer> fundosIds) {
        Utils.fundosIds = fundosIds;
    }

    public static void addFundoId(Integer id){
        if(id != null){
            fundosIds.add(id);
        }
    }

    public static Integer getFundoIdByValue(Integer n){
        return fundosIds.get(n);
    }

    public static Integer getColorId(Context context, Integer id){
        return context.getResources().getColor(id);
    }

    public static final void salvar(Context context, String key, Boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static boolean getStatus(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, true);
    }
}
